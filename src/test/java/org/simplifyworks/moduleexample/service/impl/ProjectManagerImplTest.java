package org.simplifyworks.moduleexample.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.persistence.metamodel.SingularAttribute;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.simplifyworks.core.service.SearchManager;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.core.util.Mapper;
import org.simplifyworks.core.util.Wrapper;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.moduleexample.model.dto.ProjectDto;
import org.simplifyworks.moduleexample.model.dto.ProjectFilterDto;
import org.simplifyworks.moduleexample.model.entity.Project;
import org.simplifyworks.moduleexample.model.entity.Project_;
import org.simplifyworks.moduleexample.service.ProjectManager;
import org.simplifyworks.repository.DbDataRepository;

public class ProjectManagerImplTest {

	@Mock
	private Mapper mapper;
	@Mock
	private Wrapper wrapper;

	@Mock
	private SearchManager searchManager;

	@Mock
	private DbDataRepository dataRepository;

	@Mock
	private AbstractSearchLoadWriteManager abstractSearchLoadWriteManager;

	@InjectMocks
	private final ProjectManager projectManager = new ProjectManagerImpl();

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);

		Project_.name = (SingularAttribute<Project, String>) Mockito.mock(SingularAttribute.class);
		Project_.description = (SingularAttribute<Project, String>) Mockito.mock(SingularAttribute.class);

		Project entity = new Project();
		ProjectDto dto = new ProjectDto();
		ResourceWrapper wrpDto = new ResourceWrapper(dto);

		Collection<Project> ents = new ArrayList<>();
		Collection<ResourceWrapper<ProjectDto>> wrpDtos = new ArrayList<>();
		wrpDtos.add(new ResourceWrapper<>(dto));

		when(wrapper.wrap(entity, projectManager)).thenReturn(wrpDto);
		when(wrapper.wrap(ents, 1L, 1L, projectManager)).thenReturn(new ResourcesWrapper(wrpDtos,1l,1l));
	}

	@Test
	public void testGetAll() {
		ProjectFilterDto filter = new ProjectFilterDto();
		projectManager.getAllWrapped(filter);
		verify(searchManager).getAllWrapped(filter, projectManager);
	}

	@Test
	public void testGetQuickSearchProperties() {
		when(Project_.name.getName()).thenReturn("project");
		when(Project_.description.getName()).thenReturn("description");
		String[] result = projectManager.getQuickSearchProperties();
		assertTrue(Arrays.equals(new String[]{"project", "description"}, result));
	}

	@Test
	public void testApplyFilter() {

	}

	@Test
	public void testLoad() {
		Long id = 123L;
		Project project = new Project();
		ProjectDto dto = new ProjectDto();

		when(dataRepository.find(Project.class, id)).thenReturn(project);
		when(mapper.map(project, ProjectDto.class)).thenReturn(dto);

		ResourceWrapper<ProjectDto> result = projectManager.load(id);

		assertEquals(dto, result.getResource());
		verify(dataRepository).find(Project.class, id);
		verify(mapper).map(project, ProjectDto.class);
	}

	@Test
	public void testWrite() {
		Long id = 15L;
		ProjectDto dto = new ProjectDto();
		Project project = new Project(id);

		dto.setName("name");
		dto.setDescription("description");
		when(dataRepository.find(Project.class, id)).thenReturn(project);
		when(dataRepository.update(project)).thenReturn(project);
		when(mapper.map(dto, project)).thenReturn(project);

		Long result = projectManager.write(id, dto);

		assertEquals(id, result);
		verify(dataRepository).update(project);
	}

	@Test
	public void testWriteNull() {
		Long id = null;
		ProjectDto dto = new ProjectDto();
		Project project = new Project(id);

		when(mapper.map(eq(dto),any(Project.class))).thenReturn(project);

		Long result = projectManager.write(id, dto);

		verify(dataRepository).persist(project);
	}

	@Test
	public void testCreate() {
		ProjectDto dto = new ProjectDto();
		Project project = new Project();

		when(mapper.map(eq(dto),any(Project.class))).thenReturn(project);

		projectManager.create(dto);

		verify(dataRepository).persist(project);
	}

	@Test
	public void testDelete() {
		Long id = 15L;
		projectManager.delete(id);
		verify(dataRepository).delete(Project.class, id);
	}

}
