package org.simplifyworks.moduleexample.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import org.simplifyworks.core.util.Mapper;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.core.service.SearchManager;
import org.simplifyworks.core.util.Wrapper;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.moduleexample.model.domain.PriorityEnum;
import org.simplifyworks.moduleexample.model.dto.IssueBaseInfoDto;
import org.simplifyworks.moduleexample.model.dto.IssueDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.dto.ProjectDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.model.entity.Issue_;
import org.simplifyworks.moduleexample.model.entity.Project;
import org.simplifyworks.moduleexample.model.entity.Project_;
import org.simplifyworks.moduleexample.model.entity.Task;
import org.simplifyworks.moduleexample.service.IssueBaseInfoManager;
import org.simplifyworks.moduleexample.service.IssueManager;
import org.simplifyworks.repository.DbDataRepository;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IssueManagerImplTest {

	@Mock
	private DbDataRepository dataRepository;

	@InjectMocks
	private IssueBaseInfoManager issueBaseInfoManager = new IssueBaseInfoManagerImpl();

	@Mock
	private Mapper mapper;
	@Mock
	private Wrapper wrapper;

	@Mock
	private SearchManager searchManager;
	@Mock
	private AbstractSearchLoadWriteManager abstractSearchLoadWriteManager;
	@Mock
	private SearchLoadWriteManager searchLoadWriteManager;

	@Mock
	private CriteriaBuilder criteriaBuilder;

	@Mock
	private CriteriaQuery<?> criteriaQuery;

	@Mock
	private Root<Issue> root;

	@Mock
	private Path<String> titlePath;

	@Mock
	private Path<BigDecimal> pricePath;

	@Mock
	private Expression<String> lowerTitle;

	@Mock
	private Predicate predicate;

	@Mock
	private Path<PriorityEnum> priorityPath;

	@Mock
	private Path<Boolean> internalPath;

	@Mock
	private Path<Date> dueDatePath;

	@Mock
	private Path<Long> idPath;

	@Mock
	private Join<Issue, Project> join;

	@InjectMocks
	private final IssueManager issueManager = new IssueManagerImpl();

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);

		Issue_.title = (SingularAttribute<Issue, String>) Mockito.mock(SingularAttribute.class);
		Issue_.description = (SingularAttribute<Issue, String>) Mockito.mock(SingularAttribute.class);
		Issue_.price = (SingularAttribute<Issue, BigDecimal>) Mockito.mock(SingularAttribute.class);
		Issue_.priority = (SingularAttribute<Issue, PriorityEnum>) Mockito.mock(SingularAttribute.class);
		Issue_.internal = (SingularAttribute<Issue, Boolean>) Mockito.mock(SingularAttribute.class);
		Issue_.dueDate = (SingularAttribute<Issue, Date>) Mockito.mock(SingularAttribute.class);
		Issue_.project = (SingularAttribute<Issue, Project>) Mockito.mock(SingularAttribute.class);

		Issue entity = new Issue();
		IssueDto dto = new IssueDto();
		ResourceWrapper wrpDto = new ResourceWrapper(dto);

		Collection<Issue> ents = new ArrayList<>();
		Collection<ResourceWrapper<IssueDto>> wrpDtos = new ArrayList<>();
		wrpDtos.add(new ResourceWrapper<>(dto));

		when(wrapper.wrap(entity, issueManager)).thenReturn(wrpDto);
		when(wrapper.wrap(ents, 1L, 1L, issueManager)).thenReturn(new ResourcesWrapper(wrpDtos,1l,1l));
	}

	@Test
	public void testGetAll() {
		IssueFilterDto filter = new IssueFilterDto();
		issueManager.getAllWrapped(filter);
		verify(searchManager).getAllWrapped(filter, issueManager);
	}

	@Test
	public void testGetQuickSearchProperties() {
		when(Issue_.title.getName()).thenReturn("title");
		when(Issue_.description.getName()).thenReturn("description");
		String[] result = issueManager.getQuickSearchProperties();
		assertTrue(Arrays.equals(new String[]{"title", "description"}, result));
	}

	@Test
	public void testApplyFilter_getTitle() {
		IssueFilterDto filter = new IssueFilterDto();
		filter.setTitle("Titulek");
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.title)).thenReturn(titlePath);
		when(criteriaBuilder.lower(titlePath)).thenReturn(lowerTitle);
		when(criteriaBuilder.like(lowerTitle, "%titulek%")).thenReturn(predicate);

		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.title);
		verify(criteriaBuilder).lower(titlePath);
		verify(criteriaBuilder).like(lowerTitle, "%titulek%");

	}

	@Test
	public void testApplyFilter_getMinPrice() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		BigDecimal minPrice = new BigDecimal(1);
		filter.setMinPrice(minPrice);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.price)).thenReturn(pricePath);
		when(criteriaBuilder.ge(pricePath, filter.getMinPrice())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.price);
		verify(criteriaBuilder).ge(pricePath, filter.getMinPrice());

	}

	@Test
	public void testApplyFilter_getMaxPrice() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		BigDecimal maxPrice = new BigDecimal(10);
		filter.setMaxPrice(maxPrice);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.price)).thenReturn(pricePath);
		when(criteriaBuilder.le(pricePath, filter.getMaxPrice())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.price);
		verify(criteriaBuilder).le(pricePath, filter.getMaxPrice());
	}

	@Test
	public void testApplyFilter_getPriority() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		//BLOCKER, CRITICAL, MAJOR, MINOR, TRIVIAL
		PriorityEnum priority = PriorityEnum.BLOCKER;
		filter.setPriority(priority);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.priority)).thenReturn(priorityPath);
		when(criteriaBuilder.equal(priorityPath, filter.getPriority())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.priority);
		verify(criteriaBuilder).equal(priorityPath, filter.getPriority());
	}

	@Test
	public void testApplyFilter_getInternal() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		Boolean internal = true;
		filter.setInternal(internal);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.internal)).thenReturn(internalPath);
		when(criteriaBuilder.equal(internalPath, filter.getInternal())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.internal);
		verify(criteriaBuilder).equal(internalPath, filter.getInternal());
	}

	@Test
	public void testApplyFilter_getFromDate() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		Date fromDate = new Date(500000);
		filter.setFromDate(fromDate);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.dueDate)).thenReturn(dueDatePath);
		when(criteriaBuilder.greaterThanOrEqualTo(dueDatePath, filter.getFromDate())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.dueDate);
		verify(criteriaBuilder).greaterThanOrEqualTo(dueDatePath, filter.getFromDate());
	}

	@Test
	public void testApplyFilter_getToDate() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		Date toDate = new Date(500000);
		filter.setToDate(toDate);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.get(Issue_.dueDate)).thenReturn(dueDatePath);
		when(criteriaBuilder.lessThanOrEqualTo(dueDatePath, filter.getToDate())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).get(Issue_.dueDate);
		verify(criteriaBuilder).lessThanOrEqualTo(dueDatePath, filter.getToDate());
	}

	@Test
	public void testApplyFilter_getProject() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		ProjectDto project = new ProjectDto();
		filter.setProject(project);
		List<Predicate> filterParameters = new ArrayList<>();

		when(root.join(Issue_.project)).thenReturn(join);
		when(join.get(Project_.id)).thenReturn(idPath);
		when(criteriaBuilder.equal(idPath, filter.getProject().getId())).thenReturn(predicate);

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.COUNT);

		//verification
		assertEquals(Collections.singletonList(predicate), filterParameters);
		verify(root).join(Issue_.project);
		verify(join).get(Project_.id);
		verify(criteriaBuilder).equal(idPath, filter.getProject().getId());

	}

	@Test
	public void testApplyFilterAllNull() {
		//mock and init
		IssueFilterDto filter = new IssueFilterDto();
		List<Predicate> filterParameters = new ArrayList<>();

		//method call
		issueManager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, ApplyFilterTypeEnum.RESULTS);

		//verification
		assertEquals(new ArrayList<>(), filterParameters);
		verify(root).fetch(Issue_.project);
	}

	@Test
	public void testToDto() {
		int e;
		Issue entity = new Issue();
		IssueDto dto = new IssueDto();

		//adding tasks to test x.getTasksCount()
		for (e = 0; e <= 5; e++) {
			entity.getTasks().add(new Task());
		}

		when(mapper.map(entity, IssueDto.class)).thenReturn(dto);
		IssueDto result = issueManager.toDto(entity);
		assertSame(dto, result);

		assertEquals(result.getTasksCount().intValue(), e);
	}

	@Test
	public void testToDtoNull() {
		Issue entity = null;

		IssueDto resultNull = issueManager.toDto(entity);
		verify(mapper, never()).map(entity, IssueDto.class);
		assertNull(resultNull);
	}

	@Test
	public void testCreate() {
		IssueBaseInfoDto dto = new IssueBaseInfoDto();
		Issue entity = new Issue();
		when(mapper.map(eq(dto),any(Issue.class))).thenReturn(entity);

		issueBaseInfoManager.create(dto);
		verify(dataRepository).persist(entity);
	}

	@Test
	public void testDelete() {
		issueManager.delete(15L);

		verify(dataRepository).delete(Issue.class, 15L);
	}

}
