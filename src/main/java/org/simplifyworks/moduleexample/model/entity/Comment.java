package org.simplifyworks.moduleexample.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "EXA_COMMENT")
public class Comment extends AbstractEntity {

	@JoinColumn(name = "ISSUE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private Issue issue;
	@NotNull
	@Size(max = 2000)
	@Column(name = "DESCRIPTION", length = 2000, nullable = false)
	private String description;
	@Column(name = "TIME")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date time;
	@Size(max = 100)
	@Column(name = "NICK", length = 100)
	private String nick;

	public Comment() {
	}

	public Comment(Long id) {
		super(id);
	}

	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

}
