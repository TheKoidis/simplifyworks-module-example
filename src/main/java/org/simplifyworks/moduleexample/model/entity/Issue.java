package org.simplifyworks.moduleexample.model.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.BatchSize;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.moduleexample.model.domain.EnvironmentEnum;
import org.simplifyworks.moduleexample.model.domain.PriorityEnum;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "EXA_ISSUE")
public class Issue extends AbstractEntity {

	@NotNull
	@Size(max = 50)
	@Column(name = "TITLE", length = 50, nullable = false)
	private String title;
	@NotNull
	@Size(max = 2000)
	@Column(name = "DESCRIPTION", length = 2000, nullable = false)
	private String description;
	@Column(name = "DUE_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dueDate;
	@Size(min = 2, max = 10)
	@Column(name = "REPORTED_VERSION", length = 10)
	private String reportedVersion;
	@Size(min = 2, max = 10)
	@Column(name = "FIX_VERSION", length = 10)
	private String fixVersion;
	@Size(max = 255)
	@Column(name = "REPORTER", length = 255)
	private String reporter;
	@NotNull
	@Column(name = "PRIORITY", nullable = false)
	@Enumerated(EnumType.STRING)
	private PriorityEnum priority;
	@Max(value = 99999999999999L)
	@Column(name = "PRICE", precision = 14, scale = 2)
	private BigDecimal price;
	@NotNull
	@Column(name = "INTERNAL", nullable = false)
	private Boolean internal;

	@JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private Project project;

	@OneToMany(mappedBy = "issue", cascade = CascadeType.ALL, orphanRemoval = true)
	@BatchSize(size = 100)
	private Set<Task> tasks;

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(name = "EXA_ISSUE_COMPONENTS")
	private Set<Component> components;

	@ElementCollection
	@JoinTable(name = "EXA_ISSUE_LABELS")
	private Set<String> labels;
	@ElementCollection
	@Enumerated(EnumType.STRING)
	@JoinTable(name = "EXA_ISSUE_ENVIRONMENTS")
	private Set<EnvironmentEnum> environments;

	@OneToMany(mappedBy = "issue", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Comment> comments;

	@OneToMany(mappedBy = "issue", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<ExaAttachment> attachments;

	public Issue() {
	}

	public Issue(Long id) {
		super(id);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Boolean getInternal() {
		return internal;
	}

	public void setInternal(Boolean internal) {
		this.internal = internal;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public Set<Task> getTasks() {
		if (tasks == null) {
			tasks = new LinkedHashSet<>();
		}

		return tasks;
	}

	public void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}

	public Set<Component> getComponents() {
		if (components == null) {
			components = new LinkedHashSet<>();
		}

		return components;
	}

	public void setComponents(Set<Component> components) {
		this.components = components;
	}

	public Set<String> getLabels() {
		if (labels == null) {
			labels = new LinkedHashSet<>();
		}

		return labels;
	}

	public void setLabels(Set<String> labels) {
		this.labels = labels;
	}

	public Set<EnvironmentEnum> getEnvironments() {
		if (environments == null) {
			environments = new LinkedHashSet<>();
		}

		return environments;
	}

	public void setEnvironments(Set<EnvironmentEnum> environments) {
		this.environments = environments;
	}

	public String getReportedVersion() {
		return reportedVersion;
	}

	public void setReportedVersion(String reportedVersion) {
		this.reportedVersion = reportedVersion;
	}

	public String getFixVersion() {
		return fixVersion;
	}

	public void setFixVersion(String fixVersion) {
		this.fixVersion = fixVersion;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public PriorityEnum getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnum priority) {
		this.priority = priority;
	}

	public Set<Comment> getComments() {
		if (comments == null) {
			comments = new LinkedHashSet<>();
		}

		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Set<ExaAttachment> getAttachments() {
		if (attachments == null) {
			attachments = new LinkedHashSet<>();
		}

		return attachments;
	}

	public void setAttachments(Set<ExaAttachment> attachments) {
		this.attachments = attachments;
	}

}
