package org.simplifyworks.moduleexample.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.simplifyworks.ecm.model.entity.CoreAttachment;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "EXA_ATTACHMENT")
@PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
public class ExaAttachment extends CoreAttachment {

	@JoinColumn(name = "ISSUE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private Issue issue;
	@Size(max = 255)
	@Column(name = "DESCRIPTION", length = 255)
	private String description;

	public ExaAttachment() {
	}

	public ExaAttachment(Long id) {
		super(id);
	}

	public ExaAttachment(String name, String uid, Long size, Date uploaded, String mimeType) {
		super(name, uid, size, uploaded, mimeType);
	}

	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
