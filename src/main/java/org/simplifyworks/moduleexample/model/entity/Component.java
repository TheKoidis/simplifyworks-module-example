package org.simplifyworks.moduleexample.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "EXA_COMPONENT")
public class Component extends AbstractEntity {

	@NotNull
	@Size(max = 255)
	@Column(name = "NAME", nullable = false, length = 255)
	private String name;
	@Size(max = 2000)
	@Column(name = "DESCRIPTION", length = 2000)
	private String description;

	public Component() {
	}

	public Component(Long id) {
		super(id);
	}

	public Component(Long id, String name, String description) {
		super(id);
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
