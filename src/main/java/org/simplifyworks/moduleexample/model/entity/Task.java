package org.simplifyworks.moduleexample.model.entity;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.moduleexample.model.domain.TaskTypeEnum;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "EXA_TASK")
public class Task extends AbstractEntity {

	@JoinColumn(name = "ISSUE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private Issue issue;
	@NotNull
	@Column(name = "TYPE", nullable = false)
	@Enumerated(EnumType.STRING)
	private TaskTypeEnum type;
	@Max(value = 20)
	@Column(name = "ESTIMATION", precision = 2, scale = 0)
	private Integer estimation;
	@NotNull
	@Size(max = 2000)
	@Column(name = "DESCRIPTION", length = 2000, nullable = false)
	private String description;
	@Column(name = "DUE_DATE")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date dueDate;
	@OneToOne(optional = true, cascade = CascadeType.ALL, mappedBy = "task")
	private Resolution resolution;

	public Task() {
	}

	public Task(Long id) {
		super(id);
	}

	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public TaskTypeEnum getType() {
		return type;
	}

	public void setType(TaskTypeEnum type) {
		this.type = type;
	}

	public Integer getEstimation() {
		return estimation;
	}

	public void setEstimation(Integer estimation) {
		this.estimation = estimation;
	}

	public Resolution getResolution() {
		return resolution;
	}

	public void setResolution(Resolution resolution) {
		this.resolution = resolution;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

}
