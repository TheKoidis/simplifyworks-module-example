package org.simplifyworks.moduleexample.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.moduleexample.model.domain.ResultEnum;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "EXA_RESOLUTION")
public class Resolution extends AbstractEntity {

	@JoinColumn(name = "TASK_ID", referencedColumnName = "ID")
	@OneToOne(optional = false)
	private Task task;
	@NotNull
	@Column(name = "RESULT", nullable = false)
	@Enumerated(EnumType.STRING)
	private ResultEnum result;
	@Size(max = 2000)
	@Column(name = "NOTE", length = 2000)
	private String comment;
	@Column(name = "FINISHED")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date finished;

	public Resolution() {
	}

	public Resolution(Long id) {
		super(id);
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getFinished() {
		return finished;
	}

	public void setFinished(Date finished) {
		this.finished = finished;
	}

	public ResultEnum getResult() {
		return result;
	}

	public void setResult(ResultEnum result) {
		this.result = result;
	}

}
