package org.simplifyworks.moduleexample.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum EnvironmentEnum {
	WINDOWS,
	UNIX,
	MAC,
	ANDROID
}
