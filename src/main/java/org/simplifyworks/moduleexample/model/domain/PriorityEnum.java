package org.simplifyworks.moduleexample.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum PriorityEnum {
	BLOCKER, CRITICAL, MAJOR, MINOR, TRIVIAL;
}
