package org.simplifyworks.moduleexample.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum ResultEnum {
	FIXED, REJECTED, CANCELLED, CANNOT_REPRODUCE;
}
