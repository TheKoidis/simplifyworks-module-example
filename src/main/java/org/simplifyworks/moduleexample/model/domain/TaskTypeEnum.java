package org.simplifyworks.moduleexample.model.domain;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum TaskTypeEnum {
	ANALYSING,
	CODING,
	TESTING,
	DEPLOYING;
}
