package org.simplifyworks.moduleexample.model.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IssueDto extends IssueBaseInfoDto {

	private Date dueDate;
	@Size(min = 2, max = 10)
	private String reportedVersion;
	@Size(min = 2, max = 10)
	private String fixVersion;
	@Size(max = 255)
	private String reporter;
	@Max(value = 99999999999999L)
	private BigDecimal price;
	private Integer tasksCount;

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getReportedVersion() {
		return reportedVersion;
	}

	public void setReportedVersion(String reportedVersion) {
		this.reportedVersion = reportedVersion;
	}

	public String getFixVersion() {
		return fixVersion;
	}

	public void setFixVersion(String fixVersion) {
		this.fixVersion = fixVersion;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public Integer getTasksCount() {
		return tasksCount;
	}

	public void setTasksCount(Integer tasksCount) {
		this.tasksCount = tasksCount;
	}
}
