package org.simplifyworks.moduleexample.model.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.moduleexample.model.domain.ResultEnum;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class ResolutionDto extends AbstractDto {

	@NotNull
	private ResultEnum result;
	@Size(max = 2000)
	private String comment;
	private Date finished;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getFinished() {
		return finished;
	}

	public void setFinished(Date finished) {
		this.finished = finished;
	}

	public ResultEnum getResult() {
		return result;
	}

	public void setResult(ResultEnum result) {
		this.result = result;
	}

}
