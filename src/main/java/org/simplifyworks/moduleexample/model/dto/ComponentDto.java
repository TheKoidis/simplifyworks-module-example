package org.simplifyworks.moduleexample.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class ComponentDto extends AbstractDto {

	@NotNull
	@Size(max = 255)
	private String name;
	@Size(max = 2000)
	private String description;

	public ComponentDto() {
	}

	public ComponentDto(Long id, String name, String description) {
		super(id);
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
