package org.simplifyworks.moduleexample.model.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.moduleexample.model.domain.EnvironmentEnum;

public class IssueAdditionalInfoDto extends AbstractDto {

	private Date dueDate;
	@Size(min = 2, max = 10)
	private String reportedVersion;
	@Size(min = 2, max = 10)
	private String fixVersion;
	@Size(max = 255)
	private String reporter;
	@Max(value = 99999999999999L)
	private BigDecimal price;

	private Set<TaskDto> tasks;
	private Set<ComponentDto> components;

	private Set<String> labels;
	private Set<EnvironmentEnum> environments;

	private Set<ExaAttachmentDto> attachments;

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Set<TaskDto> getTasks() {
		if (tasks == null) {
			tasks = new LinkedHashSet<>();
		}

		return tasks;
	}

	public void setTasks(Set<TaskDto> tasks) {
		this.tasks = tasks;
	}

	public Set<ComponentDto> getComponents() {
		if (components == null) {
			components = new LinkedHashSet<>();
		}

		return components;
	}

	public void setComponents(Set<ComponentDto> components) {
		this.components = components;
	}

	public Set<String> getLabels() {
		if (labels == null) {
			labels = new LinkedHashSet<>();
		}

		return labels;
	}

	public void setLabels(Set<String> labels) {
		this.labels = labels;
	}

	public Set<EnvironmentEnum> getEnvironments() {
		if (environments == null) {
			environments = new LinkedHashSet<>();
		}

		return environments;
	}

	public void setEnvironments(Set<EnvironmentEnum> environments) {
		this.environments = environments;
	}

	public String getReportedVersion() {
		return reportedVersion;
	}

	public void setReportedVersion(String reportedVersion) {
		this.reportedVersion = reportedVersion;
	}

	public String getFixVersion() {
		return fixVersion;
	}

	public void setFixVersion(String fixVersion) {
		this.fixVersion = fixVersion;
	}

	public String getReporter() {
		return reporter;
	}

	public void setReporter(String reporter) {
		this.reporter = reporter;
	}

	public Set<ExaAttachmentDto> getAttachments() {
		if (attachments == null) {
			attachments = new LinkedHashSet<>();
		}

		return attachments;
	}

	public void setAttachments(Set<ExaAttachmentDto> attachments) {
		this.attachments = attachments;
	}

}
