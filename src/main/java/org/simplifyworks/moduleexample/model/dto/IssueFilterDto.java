package org.simplifyworks.moduleexample.model.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractFilterDto;
import org.simplifyworks.moduleexample.model.domain.PriorityEnum;

/**
 *
 * @author kakrda
 */
public class IssueFilterDto extends AbstractFilterDto {

	@NotNull
	@Size(max = 50)
	private String title;
	private BigDecimal minPrice;
    private BigDecimal maxPrice;
    @NotNull     
	private PriorityEnum priority;
	@NotNull
	private Boolean internal;
    private ProjectDto project;
	private Date fromDate;
	private Date toDate;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

        public BigDecimal getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(BigDecimal minPrice) {
            this.minPrice = minPrice;
        }

        public BigDecimal getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(BigDecimal maxPrice) {
            this.maxPrice = maxPrice;
        }
        
	public PriorityEnum getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnum priority) {
		this.priority = priority;
	}

	public Boolean getInternal() {
		return internal;
	}

	public void setInternal(Boolean internal) {
		this.internal = internal;
	}

        public ProjectDto getProject() {
            return project;
        }

        public void setProject(ProjectDto project) {
            this.project = project;
        }
        
	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
