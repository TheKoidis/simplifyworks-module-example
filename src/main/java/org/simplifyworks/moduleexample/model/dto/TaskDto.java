package org.simplifyworks.moduleexample.model.dto;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.moduleexample.model.domain.TaskTypeEnum;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class TaskDto extends AbstractDto {

	@NotNull
	private TaskTypeEnum type;
	@Max(value = 20)
	private Integer estimation;
	@NotNull
	@Size(max = 2000)
	private String description;
	private Date dueDate;
	private ResolutionDto resolution;

	public TaskTypeEnum getType() {
		return type;
	}

	public void setType(TaskTypeEnum type) {
		this.type = type;
	}

	public Integer getEstimation() {
		return estimation;
	}

	public void setEstimation(Integer estimation) {
		this.estimation = estimation;
	}

	public ResolutionDto getResolution() {
		return resolution;
	}

	public void setResolution(ResolutionDto resolution) {
		this.resolution = resolution;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

}
