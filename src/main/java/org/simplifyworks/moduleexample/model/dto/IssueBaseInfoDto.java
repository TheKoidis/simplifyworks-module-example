package org.simplifyworks.moduleexample.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.moduleexample.model.domain.PriorityEnum;

public class IssueBaseInfoDto extends AbstractDto {

	@NotNull
	@Size(max = 50)
	private String title;
	@NotNull
	@Size(max = 2000)
	private String description;
	@NotNull
	private PriorityEnum priority;
	@NotNull
	private Boolean internal;
	private ProjectDto project;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getInternal() {
		return internal;
	}

	public void setInternal(Boolean internal) {
		this.internal = internal;
	}

	public ProjectDto getProject() {
		return project;
	}

	public void setProject(ProjectDto project) {
		this.project = project;
	}

	public PriorityEnum getPriority() {
		return priority;
	}

	public void setPriority(PriorityEnum priority) {
		this.priority = priority;
	}
}
