package org.simplifyworks.moduleexample.model.dto;

import java.util.LinkedHashSet;
import java.util.Set;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IssueCommentsDto extends AbstractDto {

	private Set<CommentDto> comments;

	public Set<CommentDto> getComments() {
		if (comments == null) {
			comments = new LinkedHashSet<>();
		}

		return comments;
	}

	public void setComments(Set<CommentDto> comments) {
		this.comments = comments;
	}

}
