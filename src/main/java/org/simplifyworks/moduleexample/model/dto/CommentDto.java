package org.simplifyworks.moduleexample.model.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CommentDto extends AbstractDto {

	@NotNull
	@Size(max = 2000)
	private String description;
	private Date time;
	@Size(max = 100)
	private String nick;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
}
