package org.simplifyworks.moduleexample.model.dto;

import java.util.Date;

import javax.validation.constraints.Size;

import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class ExaAttachmentDto extends CoreAttachmentDto {

	@Size(max = 255)
	private String description;

	public ExaAttachmentDto(String name, String uid, Long size, Date uploaded, String mimeType) {
		super(name, uid, size, uploaded, mimeType);
	}

	public ExaAttachmentDto() {
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
