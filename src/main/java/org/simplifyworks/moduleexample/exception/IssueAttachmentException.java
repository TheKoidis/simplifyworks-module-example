package org.simplifyworks.moduleexample.exception;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IssueAttachmentException extends RuntimeException {

	private Object[] details;
	
	public IssueAttachmentException() {
	}

	public IssueAttachmentException(Throwable cause) {
		super(cause);
	}

	public IssueAttachmentException(String message, Throwable cause) {
		super(message, cause);
	}

	public IssueAttachmentException(String message) {
		super(message);
	}
	
	public IssueAttachmentException(String message, Object[] details) {
		super(message);
		this.details = details;
	}
	
	public IssueAttachmentException(String message, Object[] details, Throwable cause) {
		super(message, cause);
		this.details = details;
	}

	public Object[] getDetails() {
		return details;
	}

	public void setDetails(Object[] details) {
		this.details = details;
	}
}
