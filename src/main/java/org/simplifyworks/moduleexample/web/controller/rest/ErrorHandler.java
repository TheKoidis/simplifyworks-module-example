package org.simplifyworks.moduleexample.web.controller.rest;

import java.util.Map;
import org.elasticsearch.common.collect.Maps;
import org.simplifyworks.core.exception.ParametrizedVndError;
import org.simplifyworks.moduleexample.exception.IssueAttachmentException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ErrorHandler {

	@ResponseStatus(value = HttpStatus.CONFLICT)
	@ExceptionHandler(IssueAttachmentException.class)
	@ResponseBody
	public VndErrors unsupportedContentType(Exception e) {
		Map<String, Object> params = Maps.newHashMap();
		params.put("properties", ((IssueAttachmentException) e).getDetails());
		return new VndErrors(new ParametrizedVndError("example-issue_attachment_unsupported", "example-issue_attachment_unsupported", "example-issue_attachment_unsupported", params, null));

	}
}
