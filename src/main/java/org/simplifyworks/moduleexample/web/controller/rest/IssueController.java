package org.simplifyworks.moduleexample.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.moduleexample.model.dto.IssueDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.service.IssueManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Issue controller
 *
 * @author kakrda
 */
@RestController
@RequestMapping(value = "/api/example/issues")
public class IssueController {

	@Autowired
	private IssueManager manager;

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ResourcesWrapper<ResourceWrapper<IssueDto>> getIssues(@Valid @RequestBody IssueFilterDto filter) {
		return manager.readWrapped(filter);
	}

}
