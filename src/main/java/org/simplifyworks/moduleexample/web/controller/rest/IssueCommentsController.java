package org.simplifyworks.moduleexample.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.client.web.controller.AbstractReadWriteController;
import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.moduleexample.model.dto.IssueCommentsDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.service.IssueCommentsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/example/issues")
public class IssueCommentsController extends AbstractReadWriteController<IssueCommentsDto, Issue, IssueFilterDto> {

	@Autowired
	private IssueCommentsManager manager;

	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/filter/comments")
	public ResourcesWrapper<ResourceWrapper<IssueCommentsDto>> read(@Valid @RequestBody IssueFilterDto filter) {
		throw new UnsupportedOperationException("Not supported yet."); //implemented in IssueController
	}

	@Override
	@RequestMapping(method = RequestMethod.POST, value = "/comments")
	public Long create(@Valid @RequestBody IssueCommentsDto dto) {
		throw new UnsupportedOperationException("Not supported yet.");  //implemented in IssueBasaeInfoController
	}

	@Override
	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}/comments")
	public void delete(@PathVariable("id") Long id) {
		throw new UnsupportedOperationException("Not supported yet."); //implemented in IssueBasaeInfoController
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/comments")
	public ResourceWrapper<IssueCommentsDto> read(@PathVariable("id") Long id) {
		return super.read(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/comments")
	public Long write(@PathVariable("id") Long id, @Valid @RequestBody IssueCommentsDto dto) {
		return super.write(id, dto);
	}

	@Override
	public SearchLoadWriteManager<IssueCommentsDto, Issue, IssueFilterDto> getManager() {
		return manager;
	}
}
