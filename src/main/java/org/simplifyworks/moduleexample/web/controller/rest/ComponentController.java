package org.simplifyworks.moduleexample.web.controller.rest;

import org.simplifyworks.client.web.controller.AbstractReadController;
import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.moduleexample.model.dto.ComponentDto;
import org.simplifyworks.moduleexample.model.dto.ComponentFilterDto;
import org.simplifyworks.moduleexample.model.entity.Component;
import org.simplifyworks.moduleexample.service.ComponentManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/example/components")
public class ComponentController extends AbstractReadController<ComponentDto, Component, ComponentFilterDto> {

	@Autowired
	private ComponentManager componentManager;

	@Override
	public SearchLoadWriteManager<ComponentDto, Component, ComponentFilterDto> getManager() {
		return componentManager;
	}

}
