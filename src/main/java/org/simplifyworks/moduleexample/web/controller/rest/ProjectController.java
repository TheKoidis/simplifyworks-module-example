package org.simplifyworks.moduleexample.web.controller.rest;

import org.simplifyworks.client.web.controller.AbstractReadWriteController;
import org.simplifyworks.moduleexample.model.dto.ProjectDto;
import org.simplifyworks.moduleexample.model.dto.ProjectFilterDto;
import org.simplifyworks.moduleexample.model.entity.Project;
import org.simplifyworks.moduleexample.service.ProjectManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.simplifyworks.core.service.SearchLoadWriteManager;

@RestController
@RequestMapping(value = "/api/example/projects")
public class ProjectController extends AbstractReadWriteController<ProjectDto, Project, ProjectFilterDto>{

	@Autowired
	private ProjectManager manager;

	@Override
	public SearchLoadWriteManager<ProjectDto, Project, ProjectFilterDto> getManager() {
		return manager;
	}
}
