package org.simplifyworks.moduleexample.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.moduleexample.model.dto.IssueAdditionalInfoDto;
import org.simplifyworks.moduleexample.service.IssueAdditionalInfoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/example/issues")
public class IssueAdditionalInfoController {

	@Autowired
	private IssueAdditionalInfoManager manager;

	@RequestMapping(method = RequestMethod.GET, value = "/{id}/additional-info")
	public ResourceWrapper<IssueAdditionalInfoDto> read(@PathVariable("id") Long id) {
		return manager.readWrapped(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/additional-info")
	public void write(@PathVariable("id") Long id, @Valid @RequestBody IssueAdditionalInfoDto dto) {
		manager.write(id, dto);
	}
}
