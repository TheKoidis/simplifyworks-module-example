package org.simplifyworks.moduleexample.web.controller.rest;

import java.io.IOException;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.simplifyworks.ecm.web.controller.rest.CoreAttachmentController;
import org.simplifyworks.moduleexample.exception.IssueAttachmentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@RestController
@RequestMapping(value = "/api/example/attachments")
public class IssueAttachmentController {

	@Autowired
	private CoreAttachmentManager attachmentManager;

	@Autowired
	private CoreAttachmentController coreAttachmentController;

	@RequestMapping(value = "/download/{uid}", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> download(@PathVariable String uid) throws IOException {
		return coreAttachmentController.download(uid);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public CoreAttachmentDto upload(MultipartFile file) throws IOException {
		Assert.notNull(file, "Insert binary data!");

		if (!file.getContentType().matches("^image/.*")) {
			throw new IssueAttachmentException("Unsupported content type", new Object[]{file.getContentType()});

		}

		return attachmentManager.saveAttachment(file);
	}

}
