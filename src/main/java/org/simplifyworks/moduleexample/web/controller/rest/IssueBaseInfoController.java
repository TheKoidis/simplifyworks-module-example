package org.simplifyworks.moduleexample.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.client.web.controller.AbstractReadWriteController;
import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.core.web.domain.ResourceWrapper;
import org.simplifyworks.core.web.domain.ResourcesWrapper;
import org.simplifyworks.moduleexample.model.dto.IssueBaseInfoDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.service.IssueBaseInfoManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/example/issues")
public class IssueBaseInfoController extends AbstractReadWriteController<IssueBaseInfoDto, Issue, IssueFilterDto> {

	@Autowired
	private IssueBaseInfoManager manager;

	@Override
	@RequestMapping //(method = RequestMethod.POST, value = "/filter")
	public ResourcesWrapper<ResourceWrapper<IssueBaseInfoDto>> read(@Valid @RequestBody IssueFilterDto filter) {
		throw new UnsupportedOperationException("Not supported yet."); //implemented in IssueController
	}

	@Override
	@RequestMapping(method = RequestMethod.GET, value = "/{id}/base-info")
	public ResourceWrapper<IssueBaseInfoDto> read(@PathVariable("id") Long id) {
		return super.read(id);
	}

	@Override
	@RequestMapping(method = RequestMethod.PUT, value = "/{id}/base-info")
	public Long write(@PathVariable("id") Long id, @Valid @RequestBody IssueBaseInfoDto dto) {
		return super.write(id, dto);
	}

	@Override
	public SearchLoadWriteManager<IssueBaseInfoDto, Issue, IssueFilterDto> getManager() {
		return manager;
	}

}
