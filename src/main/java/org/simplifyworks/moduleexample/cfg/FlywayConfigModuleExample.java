/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.moduleexample.cfg;

import org.flywaydb.core.Flyway;
import org.simplifyworks.client.config.flyway.AbstractFlywayConfig;
import org.simplifyworks.client.config.flyway.FlywayBeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

/**
 * SIFY-132, SIFY-260 multi-module db migrations
 *
 * @author hanak@ders.cz
 */
@Configuration
@ConditionalOnClass(Flyway.class)
@ConditionalOnProperty(prefix = "flyway", name = "enabled", matchIfMissing = false)
@AutoConfigureAfter(FlywayBeanPostProcessor.FlywayConfigurationExt.class)
@EnableConfigurationProperties(FlywayProperties.class)
@PropertySource("classpath:/flyway-moduleexample.properties")
public class FlywayConfigModuleExample extends AbstractFlywayConfig {

	@DependsOn("flywayCore")
	@ConditionalOnMissingBean(name = "flywayModuleExample")
	@ConditionalOnExpression("${flyway.enabled:true} && '${flyway.moduleexample.locations}'!=''")
	@ConfigurationProperties(prefix = "flyway.moduleexample")
	@Bean
	public Flyway flywayModuleExample() {
		return super.createFlyway();
	}

}
