package org.simplifyworks.moduleexample.service;

import org.simplifyworks.moduleexample.model.dto.ComponentDto;
import org.simplifyworks.moduleexample.model.dto.ComponentFilterDto;
import org.simplifyworks.moduleexample.model.entity.Component;
import org.simplifyworks.core.service.SearchLoadWriteManager;

/**
 *
 * @author Zavrel
 */
public interface ComponentManager extends SearchLoadWriteManager<ComponentDto, Component, ComponentFilterDto> {

}
