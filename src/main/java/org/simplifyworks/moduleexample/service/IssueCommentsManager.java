package org.simplifyworks.moduleexample.service;

import org.simplifyworks.moduleexample.model.dto.IssueCommentsDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.core.service.SearchLoadWriteManager;

/**
 *
 * @author Zavrel
 */
public interface IssueCommentsManager extends SearchLoadWriteManager<IssueCommentsDto, Issue, IssueFilterDto> {
}
