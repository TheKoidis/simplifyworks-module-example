package org.simplifyworks.moduleexample.service;

import org.simplifyworks.moduleexample.model.dto.ProjectDto;
import org.simplifyworks.moduleexample.model.dto.ProjectFilterDto;
import org.simplifyworks.moduleexample.model.entity.Project;
import org.simplifyworks.core.service.SearchLoadWriteManager;

/**
 *
 * @author kakrda
 */
public interface ProjectManager extends SearchLoadWriteManager<ProjectDto, Project, ProjectFilterDto> {

}
