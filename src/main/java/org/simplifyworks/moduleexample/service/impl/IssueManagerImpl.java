package org.simplifyworks.moduleexample.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractResourceWrappedEntityManager;
import org.simplifyworks.moduleexample.model.dto.IssueDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.model.entity.Issue_;
import org.simplifyworks.moduleexample.model.entity.Project;
import org.simplifyworks.moduleexample.model.entity.Project_;
import org.simplifyworks.moduleexample.service.IssueManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kakrda
 */
@Service
@Transactional
public class IssueManagerImpl extends AbstractResourceWrappedEntityManager<IssueDto, Issue, IssueFilterDto> implements IssueManager {

	@Override
	public String[] getQuickSearchProperties() {
		String[] quickSearchProperties = {Issue_.title.getName(), Issue_.description.getName()};
		return quickSearchProperties;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<Issue> root, IssueFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//title
		if (!StringUtils.isEmpty(filter.getTitle())) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(Issue_.title)),
											"%" + filter.getTitle().toLowerCase() + "%"
							)
			);
		}
		//minPrice
		if (filter.getMinPrice() != null) {
			filterParameters.add(
							criteriaBuilder.ge(
											root.get(Issue_.price),
											filter.getMinPrice()
							)
			);
		}
		//maxPrice
		if (filter.getMaxPrice() != null) {
			filterParameters.add(
							criteriaBuilder.le(
											root.get(Issue_.price),
											filter.getMaxPrice()
							)
			);
		}
		//Enum
		if (filter.getPriority() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(Issue_.priority),
											filter.getPriority()
							)
			);
		}
		//Boolean
		if (filter.getInternal() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(Issue_.internal),
											filter.getInternal()
							)
			);
		}
		//fromDate
		if (filter.getFromDate() != null) {
			filterParameters.add(
							criteriaBuilder.greaterThanOrEqualTo(
											root.get(Issue_.dueDate),
											filter.getFromDate()
							)
			);
		}
		//toDate
		if (filter.getToDate() != null) {
			filterParameters.add(
							criteriaBuilder.lessThanOrEqualTo(
											root.get(Issue_.dueDate),
											filter.getToDate()
							)
			);
		}
		//project
		if (filter.getProject() != null) {
			Join<Issue, Project> join = root.join(Issue_.project);
			filterParameters.add(
							criteriaBuilder.equal(
											join.get(Project_.id),
											filter.getProject().getId()
							)
			);
		}

		if (filterType == ApplyFilterTypeEnum.RESULTS) {
			root.fetch(Issue_.project);
			//root.fetch(Issue_.tasks, JoinType.LEFT).fetch(Task_.resolution, JoinType.LEFT);
		}
	}

	@Override
	public IssueDto toDto(Issue entity) {
		IssueDto dto = super.toDto(entity);

		if (dto != null) {
			dto.setTasksCount(entity.getTasks().size());
		}

		return dto;
	}

}
