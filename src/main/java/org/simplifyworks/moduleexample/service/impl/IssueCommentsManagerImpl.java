package org.simplifyworks.moduleexample.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang.ArrayUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.moduleexample.model.dto.IssueCommentsDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Comment;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.service.IssueCommentsManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IssueCommentsManagerImpl extends AbstractSearchLoadWriteManager<IssueCommentsDto, Issue, IssueFilterDto> implements IssueCommentsManager {

	@Override
	public Long write(Long id, IssueCommentsDto dto) {
		Issue issue = (id == null ? new Issue() : dataRepository.find(Issue.class, id));

		mapper.map(dto, issue);

		for (Comment commentEntity : issue.getComments()) {
			commentEntity.setIssue(issue);
		}

		if (issue.getId() == null) {
			dataRepository.persist(issue);
		} else {
			issue = dataRepository.update(issue);
		}

		return issue.getId();
	}

	@Override
	public String[] getQuickSearchProperties() {
		return ArrayUtils.EMPTY_STRING_ARRAY;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<Issue> root, IssueFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
	}
}
