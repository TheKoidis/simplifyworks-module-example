package org.simplifyworks.moduleexample.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang.ArrayUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractResourceWrappedEntityManager;
import org.simplifyworks.moduleexample.model.dto.IssueAdditionalInfoDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.ExaAttachment;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.model.entity.Task;
import org.simplifyworks.moduleexample.service.IssueAdditionalInfoManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IssueAdditionalInfoManagerImpl extends AbstractResourceWrappedEntityManager<IssueAdditionalInfoDto, Issue, IssueFilterDto>
				implements IssueAdditionalInfoManager {

	@Override
	public Long write(Long id, IssueAdditionalInfoDto dto) {
		Issue issue = (id == null ? new Issue() : dataRepository.find(Issue.class, id));

		mapper.map(dto, issue);

		for (ExaAttachment attachmentEntity : issue.getAttachments()) {
			attachmentEntity.setIssue(issue);
		}
		for (Task taskEntity : issue.getTasks()) {
			if (taskEntity.getResolution() != null) {
				taskEntity.getResolution().setTask(taskEntity);
			}
			taskEntity.setIssue(issue);
		}

		if (issue.getId() == null) {
			dataRepository.persist(issue);
		} else {
			issue = dataRepository.update(issue);
		}

		return issue.getId();
	}

	@Override
	public String[] getQuickSearchProperties() {
		return ArrayUtils.EMPTY_STRING_ARRAY;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<Issue> root, IssueFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
	}
}
