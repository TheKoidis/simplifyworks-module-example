package org.simplifyworks.moduleexample.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.moduleexample.model.dto.ComponentDto;
import org.simplifyworks.moduleexample.model.dto.ComponentFilterDto;
import org.simplifyworks.moduleexample.model.entity.Component;
import org.simplifyworks.moduleexample.model.entity.Component_;
import org.simplifyworks.moduleexample.service.ComponentManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ComponentManagerImpl extends AbstractSearchLoadWriteManager<ComponentDto, Component, ComponentFilterDto> implements ComponentManager {

	@Override
	public String[] getQuickSearchProperties() {
		return new String[]{Component_.name.getName(), Component_.description.getName()};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<Component> root, ComponentFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
	}
}
