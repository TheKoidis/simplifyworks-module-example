package org.simplifyworks.moduleexample.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.ArrayUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.moduleexample.model.dto.IssueBaseInfoDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.moduleexample.service.IssueBaseInfoManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class IssueBaseInfoManagerImpl extends AbstractSearchLoadWriteManager<IssueBaseInfoDto, Issue, IssueFilterDto> implements IssueBaseInfoManager {

	@Override
	public String[] getQuickSearchProperties() {
		return ArrayUtils.EMPTY_STRING_ARRAY;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<Issue> root, IssueFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
	}
}
