package org.simplifyworks.moduleexample.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.moduleexample.model.dto.ProjectDto;
import org.simplifyworks.moduleexample.model.dto.ProjectFilterDto;
import org.simplifyworks.moduleexample.model.entity.Project;
import org.simplifyworks.moduleexample.model.entity.Project_;
import org.simplifyworks.moduleexample.service.ProjectManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProjectManagerImpl extends AbstractSearchLoadWriteManager<ProjectDto, Project, ProjectFilterDto> implements ProjectManager {

	@Override
	public String[] getQuickSearchProperties() {
		return new String[]{Project_.name.getName(), Project_.description.getName()};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<Project> root, ProjectFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
	}

}
