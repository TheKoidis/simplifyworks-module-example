package org.simplifyworks.moduleexample.service;

import org.simplifyworks.core.service.ResourceWrappedEntityManager;
import org.simplifyworks.moduleexample.model.dto.IssueDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;

/**
 * Issue manager
 *
 * @author kakrda
 */
public interface IssueManager extends ResourceWrappedEntityManager<IssueDto, Issue, IssueFilterDto> {
}
