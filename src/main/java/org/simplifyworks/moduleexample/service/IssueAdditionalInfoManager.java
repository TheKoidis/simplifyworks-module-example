package org.simplifyworks.moduleexample.service;

import org.simplifyworks.core.service.ResourceWrappedEntityManager;
import org.simplifyworks.moduleexample.model.dto.IssueAdditionalInfoDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;

/**
 *
 * @author Zavrel
 */
public interface IssueAdditionalInfoManager extends ResourceWrappedEntityManager<IssueAdditionalInfoDto, Issue, IssueFilterDto>{

}
