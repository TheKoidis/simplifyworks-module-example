package org.simplifyworks.moduleexample.service;

import org.simplifyworks.moduleexample.model.dto.IssueBaseInfoDto;
import org.simplifyworks.moduleexample.model.dto.IssueFilterDto;
import org.simplifyworks.moduleexample.model.entity.Issue;
import org.simplifyworks.core.service.SearchLoadWriteManager;

/**
 *
 * @author Zavrel
 */
public interface IssueBaseInfoManager extends SearchLoadWriteManager<IssueBaseInfoDto, Issue, IssueFilterDto> {

}
