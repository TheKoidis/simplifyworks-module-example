var TodoConstants = {
    TODO_ADD: 'TODO_ADD',
    TODO_CLEAR: 'TODO_CLEAR'
};

module.exports = TodoConstants;
