var IssueDetailTitle = React.createClass({

	mixins: [FluxMixin, StoreWatchMixin('form')],

	contextTypes: {
		flux: React.PropTypes.object.isRequired,
	},

	getStateFromFlux: function() {
		var isNew = this.props.params.issueId === 'new';

		return isNew ? {} : this.context.flux.store('form').getForm('/api/example/issues/' + this.props.params.issueId + '/base-info');
	},

	render: function() {
		return RenderUtils.renderDetailTitle('example-issue', this.props.params.issueId, this.state.data ? this.state.data.title : null);
	}
});

var IssueDetail = React.createClass({

	contextTypes: {
		router: React.PropTypes.object.isRequired,
	},

	renderProject: function(project) {
		return project ? (project.name + ' (' + project.description + ')') : '';
	},

	renderResolution: function(resolution) {
		return resolution ? resolution.result : '';
	},

	renderComponent: function(component) {
		return component ? component.name : '';
	},

	renderTask: function(task) {
		return (task
			? 	<Label bsStyle={this.getTaskStyle(task.type)}>
					{task.type + ' - ' + task.description}
				</Label>
			:	''
		);
	},

	renderComment: function(comment) {
		return (comment
			? 	<Label>
					{comment.nick}
				</Label>
			:	''
		);
	},

	getTaskStyle: function(type) {
		switch(type) {
			case 'ANALYSING':
				return 'primary';
			case 'CODING':
				return 'info';
			case 'TESTING':
				return 'warning';
			case 'DEPLOYING':
				return 'danger';
			default:
				return 'default';
		}
	},

	newCallback: function(type, id) {
		if (type == FormConstants.FORM_CREATE && id) {
			this.context.router.push('/example/issue/' + id);
		}
	},

	validateForm: function(data, callback) {
		// here some validation and notification can be added

		callback(true);
	},

	validateEmbeddableForm: function(data, callback) {
		// here some validation and notification can be added

		callback(true);
	},

	getInitialValue: function() {
		return {
			priority: 'TRIVIAL',
			description: 'Some nice description is here'
		};
	},

	getTitleLabel: function() {
		return (
			<span>
				<Glyphicon glyph='star'/>{' ' + i18n.t('example-issue-title')}
			</span>
		);
	},

	getTitleHelp: function() {
		return (
			<Image src='http://www.simplifyworks.org/icon/simplifylogo.svg' responsive />
		);
	},

	getFileSize: function(resource, property) {
		return <div style={{textAlign:'right',whiteSpace:'nowrap'}}>{RenderUtils.getFileSize(resource.resource[property], 1)}</div>;
	},

	formatSize: function(value) {
		return RenderUtils.getFileSize(value, 9);
	},

	/**
	 * Example of custom parse
	 */
	parseSize: function(text) {
		if (text===null) {
			return null;
		}
		var patt = /(.*) ([kMG]?B)$/g;
		var result = patt.exec(text);
		if (result===null) {
			return null;
		}

		var multiple = 1;
		if (result[2] == 'kB') {
			multiple = 1000;
		}
		if (result[2] == 'MB') {
			multiple = 1000000;
		}
		if (result[2] == 'GB') {
			multiple = 1000000000;
		}

		var value = NumberUtils.parse(result[1]);
		if (value===null) {
			return null;
		}

		return multiple * value;
	},

	renderDetailColumn: function(resource) {
		return i18n.t('example-attachment-detail');
	},

	render: function() {
		var isNew = this.props.params.issueId === 'new';

		return (
			<SW.Detail exitPath='/example/issues' route={this.props.route}>
				<SW.Detail.Tab labelKey='example-issue-base-info'>
					<SW.Form rest={isNew ? '/api/example/issues' : ('/api/example/issues/' + this.props.params.issueId + '/base-info')} new={isNew} newCallback={this.newCallback} labelKeyPrefix='example-issue' validate={this.validateForm} initialValue={this.getInitialValue()}>
						<SW.Form.Section>
							<SW.Form.String size={4} property='title' validations="required;length(50)" label={this.getTitleLabel()} help={this.getTitleHelp()}/>

							<SW.Form.Entity size={2} property='project' render={this.renderProject} validations='required' help={<p>Some help should be here...</p>}>
								<SW.List.EntityList name='example-project' rest='/api/example/projects'>
									<SW.List.Table>
										<SW.List.String property='name' width='15%'/>
										<SW.List.String property='description' width='85%'/>
									</SW.List.Table>

									<SW.List.QuickSearch />
									<SW.List.Info />
									<SW.List.Pagination />
								</SW.List.EntityList>
							</SW.Form.Entity>

							<SW.Form.EntitySelect size={2} property='project' render={this.renderProject} validations='required' rest='/api/example/projects'/>

							<SW.Form.Enum size={2} property='priority' options={{enum: 'Priority', values: ['BLOCKER', 'CRITICAL', 'MAJOR', 'MINOR', 'TRIVIAL']}} validations='required'/>
							<SW.Form.Boolean size={2} property='internal' validations='required' />
							<SW.Form.Space size={2} />

							<SW.Form.String size={12} property='description' type='area' validations="required;length(2000)"/>
							{/*<SW.Form.Html size={12} property='description' validations="required;length(2000)"/>*/}
						</SW.Form.Section>
					</SW.Form>
				</SW.Detail.Tab>

				{!isNew
					?	<SW.Detail.Tab labelKey='example-issue-additional-info'>
							<SW.Form rest={'/api/example/issues/' + this.props.params.issueId + '/additional-info'} labelKeyPrefix='example-issue'>
								<SW.Form.Section>
									<SW.Form.String size={2} property='reportedVersion' validations="lengthRange(2,10)"/>
									<SW.Form.String size={2} property='fixVersion' validations="lengthRange(2,10)"/>
									<SW.Form.Datetime size={2} property='dueDate' timeEnabled={false}/>

									<SW.Form.Enums size={6} property='environments' validations='required;count(2)' options={{enum: 'Environment', values: ['WINDOWS', 'UNIX', 'MAC', 'ANDROID']}}/>

									<SW.Form.Entities size={6} property='components' render={this.renderComponent} validations='required;count(5)'>
										<SW.List.EntityList name='example-component' rest='/api/example/components'>
											<SW.List.Table>
												<SW.List.String property='name' width='15%'/>
												<SW.List.String property='description' width='85%'/>
											</SW.List.Table>

											<SW.List.QuickSearch />
											<SW.List.Info />
											<SW.List.Pagination />
										</SW.List.EntityList>

										<SW.Form.EntitiesList name='example-component'>
											<SW.List.Table>
												<SW.List.String property='name' width='20%' pathProperty='id'/>
												<SW.List.String property='description' width='70%'/>
											</SW.List.Table>

											<SW.List.Info />
											<SW.List.Pagination />
										</SW.Form.EntitiesList>
									</SW.Form.Entities>

									<SW.Form.Space size={6}/>

									<SW.Form.Entities size={6} property='components' render={this.renderComponent} validations='required;count(5)'>
										<SW.List.EntityList name='example-component' rest='/api/example/components'>
											<SW.List.Table>
												<SW.List.String property='name' width='15%'/>
												<SW.List.String property='description' width='85%'/>
											</SW.List.Table>

											<SW.List.QuickSearch />
											<SW.List.Info />
											<SW.List.Pagination />
										</SW.List.EntityList>

										<SW.Form.EntitiesTagList render={this.renderComponent} />
									</SW.Form.Entities>

									<SW.Form.Space size={6}/>

									<SW.Form.Number size={2} property='price' validations='maxScale(2)'/>

									<SW.Form.Embeddables size={12} property='tasks' initialValue={{type: 'TESTING', estimation: 10}} help={<p>Some help should be here...</p>}>
										<SW.Form.EmbeddableForm name='example-task' validate={this.validateEmbeddableForm}>
											<SW.Form.Section>
												<SW.Form.Enum size={4} property='type' options={{enum: 'TaskType', values: ['ANALYSING', 'CODING', 'TESTING', 'DEPLOYING']}} validations='required'/>
												<SW.Form.Number size={4} property='estimation' validations='integer;min(1);max(20)'/>
												<SW.Form.String size={12} property='description' type='area' validations="required;length(2000)"/>
												<SW.Form.Datetime size={4} property='dueDate' timeEnabled={false}/>

												<SW.Form.Embeddable size={8} property='resolution' render={this.renderResolution} initialValue={{result: 'FIXED'}} help={<p>Some help should be here...</p>}>
													<SW.Form.EmbeddableForm name='example-resolution'>
														<SW.Form.Section>
															<SW.Form.Enum size={4} property='result' options={{enum: 'Result', values: ['FIXED', 'REJECTED', 'CANCELLED', 'CANNOT_REPRODUCE']}} validations='required'/>
															<SW.Form.String size={12} property='comment' type='area' validations="required;length(2000)"/>
															<SW.Form.Datetime size={4} property='finished' timeEnabled={false}/>
														</SW.Form.Section>
													</SW.Form.EmbeddableForm>
												</SW.Form.Embeddable>
											</SW.Form.Section>
										</SW.Form.EmbeddableForm>

										<SW.Form.EmbeddablesList name='example-task'>
											<SW.List.Table>
												<SW.List.Enum property='type' width='20%' pathProperty='id' options={{enum: 'TaskType', values: ['ANALYSING', 'CODING', 'TESTING', 'DEPLOYING']}}/>
												<SW.List.Number property='estimation' width='20%'/>
												<SW.List.String property='description' width='50%'/>
												<SW.List.Datetime property='dueDate' timeEnabled={false} width='10%'/>
											</SW.List.Table>

											<SW.List.Info />
											<SW.List.Pagination />
										</SW.Form.EmbeddablesList>
									</SW.Form.Embeddables>

									<SW.Form.Embeddables size={12} property='tasks' initialValue={{type: 'CODING'}}>
										<SW.Form.EmbeddableForm name='example-task' validate={this.validateEmbeddableForm}>
											<SW.Form.Section>
												<SW.Form.Enum size={4} property='type' options={{enum: 'TaskType', values: ['ANALYSING', 'CODING', 'TESTING', 'DEPLOYING']}}/>
												<SW.Form.Number size={4} property='estimation' validations='integer;min(1);max(500)'/>
												<SW.Form.String size={12} property='description' type='area' validations="required;length(2000)"/>
												<SW.Form.Datetime size={4} property='dueDate' timeEnabled={false}/>

												<SW.Form.Embeddable size={8} property='resolution' render={this.renderResolution} initialValue={{result: 'CANCELLED'}}>
													<SW.Form.EmbeddableForm name='example-resolution'>
														<SW.Form.Section>
															<SW.Form.Enum size={4} property='result' options={{enum: 'Result', values: ['FIXED', 'REJECTED', 'CANCELLED', 'CANNOT_REPRODUCE']}} validations='required'/>
															<SW.Form.String size={12} property='comment' type='area' validations="required;length(2000)"/>
															<SW.Form.Datetime size={4} property='finished' timeEnabled={false}/>
														</SW.Form.Section>
													</SW.Form.EmbeddableForm>
												</SW.Form.Embeddable>
											</SW.Form.Section>
										</SW.Form.EmbeddableForm>

										<SW.Form.EmbeddablesTagList render={this.renderTask} />
									</SW.Form.Embeddables>

									<SW.Form.Strings size={12} property='labels' validations='countRange(2,5)'/>

									<SW.Form.AttachmentsSimple size={6} property='attachments' validations="required" multiple={true} rest='/api/example/attachments' accept='image/png'/>

									<SW.Form.Space size={6}/>

									<SW.Form.Attachments size={6} property='attachments' labelKey='example-issue-attachments' multiple={false} rest='/api/example/attachments'>
										<SW.Form.EmbeddableForm name='example-attachment'>
											<SW.Form.Section>
												<SW.Form.String size={4} property='name' disabled={true}/>
												<SW.Form.FormattedString size={4} property='size' disabled={true} format={this.formatSize}/>
												<SW.Form.Datetime size={4} property='uploaded' timeEnabled={true} disabled={true}/>
												<SW.Form.String size={12} property='description' type='area' validations="length(255)"/>
											</SW.Form.Section>
										</SW.Form.EmbeddableForm>

										<SW.Form.AttachmentsList name='example-attachment'>
											<SW.List.Table>
												<SW.List.Attachment property='name' rest='/api/example/attachments'/>
												<SW.List.Custom property='name' pathProperty='id' orderable={false} labelKey='example-attachment-detail' render={this.renderDetailColumn}/>
												<SW.List.Custom property='size' width='10%' render={this.getFileSize}/>
												<SW.List.Datetime property='uploaded' timeEnabled={true} width='20%'/>
												<SW.List.String property='description' width='10%'/>
											</SW.List.Table>

											<SW.List.Info />
											<SW.List.Pagination />
										</SW.Form.AttachmentsList>
									</SW.Form.Attachments>
								</SW.Form.Section>
							</SW.Form>
						</SW.Detail.Tab>
					:	null
				}

				{!isNew
					?	<SW.Detail.Tab labelKey='example-issue-comments'>
							<SW.Form rest={'/api/example/issues/' + this.props.params.issueId + '/comments'} labelKeyPrefix='example-issue'>
								<SW.Form.Section>
									<SW.Form.Embeddables size={12} property='comments' initialValue={{nick: 'Martin'}}>
										<SW.Form.EmbeddableForm name='example-comment'>
											<SW.Form.Section>
												<SW.Form.String size={6} property='nick' validations="required;length(40)"/>
												<SW.Form.String size={12} property='description' type='area' validations="required;length(2000)"/>
												<SW.Form.Datetime size={4} property='time' timeEnabled={true}/>
											</SW.Form.Section>
										</SW.Form.EmbeddableForm>

										<SW.Form.EmbeddablesList name='example-comment'>
											<SW.List.Table>
												<SW.List.String property='nick' width='20%'/>
												<SW.List.String property='description' width='70%'/>
												<SW.List.Datetime property='time' timeEnabled={true} width='10%'/>
											</SW.List.Table>

											<SW.List.Info />
											<SW.List.Pagination />
										</SW.Form.EmbeddablesList>
									</SW.Form.Embeddables>

									<SW.Form.Embeddables size={12} property='comments' initialValue={{nick: 'Martin'}}>
										<SW.Form.EmbeddableForm name='example-comment'>
											<SW.Form.Section>
												<SW.Form.String size={6} property='nick' validations="required;length(40)"/>
												<SW.Form.String size={12} property='description' type='area' validations="required;length(2000)"/>
												<SW.Form.Datetime size={4} property='time' timeEnabled={true}/>
											</SW.Form.Section>
										</SW.Form.EmbeddableForm>

										<SW.Form.EmbeddablesTagList render={this.renderComment} />
									</SW.Form.Embeddables>
									<SW.Form.Custom size={10}>
									<h3>{i18n.t('example-issue-title')}</h3>
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus faucibus molestie nisl. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Proin mattis lacinia justo. Etiam egestas wisi a erat. Etiam posuere lacus quis dolor. Nulla pulvinar eleifend sem. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Aenean placerat. Etiam sapien elit, consequat eget, tristique non, venenatis quis, ante. Maecenas lorem.</p>
									</SW.Form.Custom>	
								</SW.Form.Section>
							</SW.Form>
						</SW.Detail.Tab>
					:	null
				}
			</SW.Detail>
		);
	}
});

module.exports = {title: IssueDetailTitle, main: IssueDetail};
