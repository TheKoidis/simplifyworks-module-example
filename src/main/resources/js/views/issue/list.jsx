var IssueListTitle = React.createClass({

	render: function() {
		return (
			<span>
				{i18n.t('example-issue-list')}
			</span>
		);
	}
});

var IssueList = React.createClass({

	renderColumn: function(resource, property) {
		var value = resource.resource[property];

		return <Label bsStyle={this.priorityStyle(value)}>{value}</Label>
	},

	priorityStyle: function(value) {
		switch(value) {
			case 'BLOCKER':
				return 'danger'
			case 'CRITICAL':
				return 'warning'
			case 'MAJOR':
				return 'info';
			case 'MINOR':
				return 'success'
			case 'TRIVIAL':
				return 'default'
		}

		return 'primary';
	},

	isActionEnabled: function(min, max, resources) {
		return resources.length >= min && resources.length <= max;
	},

	executeAction: function(resources) {
		notification.showLocalizedMessage('success', 'action-executed', resources.map(function(e) {
			return e.resource.title;
		}).join(', '));
	},

	renderSum: function(selection, property) {
		var sum = 0;
		if (selection) {
			selection.map(function(record, index, array) {
				sum += record.resource[property];
			});
		}

		return i18n.t('example-issue-price-sum')+': ' + NumberUtils.format(sum);
	},

	renderAvg: function(selection, property) {
		var avg = 0;
		var count = 0;
		if (selection) {
			selection.map(function(record, index, array) {
				avg += record.resource[property];
				count++;
			});
		}

		return i18n.t('example-issue-price-avg')+': ' + NumberUtils.format(avg/(count?count:1));
	},

	renderInternal: function(selection, property) {
		var count = 0;
		if (selection) {
			selection.map(function(record, index, array) {
				if (record.resource[property]) {
					count++;
				};
			});
		}

		return i18n.t('example-issue-internal')+': ' + count;
	},

	renderPublic: function(selection, property) {
		var count = 0;
		if (selection) {
			selection.map(function(record, index, array) {
				if (!record.resource[property]) {
					count++;
				};
			});
		}

		return i18n.t('example-issue-public')+': ' + count;
	},

	renderPriorities: function(selection, property) {
		var count = {TRIVIAL: 0, MINOR: 0, MAJOR: 0,  CRITICAL: 0, BLOCKER: 0};
		if (selection) {
			selection.map(function(record, index, array) {
				count[record.resource[property]]++;
			});
		}

		var labels = [];
		Object.keys(count).forEach(function(key,index) {
			labels.push(this.renderPriority(key, count[key]), ' ');
		}.bind(this));

		return <div>{labels}</div>
	},

	renderPriority: function(priority, count) {
		return <Label key={priority} bsStyle={this.priorityStyle(priority)} title={priority}>{count}</Label>
	},

	renderProject: function(project) {
		return project ? (project.name + ' (' + project.description + ')') : '';
	},

	render: function() {
		return (
			<SW.List name='example-issue' rest='/api/example/issues' defaultOrdering={[{property: 'title', order: 'asc'}, {property: 'created', order: 'desc'}]}>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.String property='title' width='15%' minWidth="30px" maxWidth='80em' path='/example/issue' pathProperty='id'/>
					<SW.List.String property='project.name' width='15%'/>
					<SW.List.Datetime property='dueDate' width='15%' timeEnabled={false}/>
					<SW.List.Custom property='priority' width='15%' orderable={false} render={this.renderColumn} renderSummary={[this.renderPriorities]}/>
					<SW.List.Boolean property='internal' width='5%' orderable={false} renderSummary={[this.renderInternal, this.renderPublic]}/>
					<SW.List.Number property='price' width='5%' renderSummary={[this.renderSum, this.renderAvg]}/>
					<SW.List.Datetime property='created' timeEnabled={true}/>
				</SW.List.Table>

				<SW.List.ActivitiesGroup>
					<SW.List.Link icon='plus' labelKey='test-create-new' path={'/example/issue/new'}/>
				</SW.List.ActivitiesGroup>

				<SW.List.ActivitiesGroup>
					<SW.List.Action icon='refresh' labelKey='test-action-1' isEnabled={this.isActionEnabled.bind(this, 0, 3)} execute={this.executeAction} />
				</SW.List.ActivitiesGroup>

				<SW.List.ActivitiesGroup>
					<SW.List.ActivitiesMenu labelKey='test-action-menu'>
						<SW.List.Action labelKey='test-action-2' isEnabled={this.isActionEnabled.bind(this, 1, 4)} execute={this.executeAction} />
						<SW.List.Action icon='certificate' labelKey='test-action-3' isEnabled={this.isActionEnabled.bind(this, 3, 20)} execute={this.executeAction} />
					</SW.List.ActivitiesMenu>
				</SW.List.ActivitiesGroup>

				<SW.List.FiltersGroup>
					<SW.List.NamedFilter labelKey='test-filter-1' execute={this.executeAction} />
					<SW.List.NamedFilter icon='certificate' labelKey='test-filter-2' execute={this.executeAction} />
				</SW.List.FiltersGroup>

				<SW.List.Filter name='filter' labelKeyPrefix='example-issue-filter'>
					<SW.Form.Section>
						<SW.Form.String size={6} property='title' validations='lengthRange(1,50)'/>
						<SW.Form.Entity size={2} property='project' render={this.renderProject}>
							<SW.List.EntityList name='example-project' rest='/api/example/projects'>
								<SW.List.Table>
									<SW.List.String property='name' width='15%'/>
									<SW.List.String property='description' width='85%'/>
								</SW.List.Table>
								<SW.List.Info />
								<SW.List.Pagination />
							</SW.List.EntityList>
						</SW.Form.Entity>
						<SW.Form.Enum size={2} property='priority' options={{enum: 'Priority', values: ['BLOCKER', 'CRITICAL', 'MAJOR', 'MINOR', 'TRIVIAL']}}/>
						<SW.Form.Boolean size={2} property='internal'/>
						<SW.Form.Number size={2} property='minPrice' validations='maxScale(2)'/>
						<SW.Form.Number size={2} property='maxPrice' validations='maxScale(2)'/>
						<SW.Form.Datetime size={2} property='fromDate'/>
						<SW.Form.Datetime size={2} property='toDate'/>
					</SW.Form.Section>
				</SW.List.Filter>

				<SW.List.QuickSearch />
				<SW.List.Pagination />
				<SW.List.Info />
			</SW.List>
		);
	}
});

module.exports = {title: IssueListTitle, main: IssueList};
