var TodoListTitle = React.createClass({

    mixins: [FluxMixin, StoreWatchMixin('todo')],

    getStateFromFlux: function() {
        return this.context.flux.store('todo').getState();
    },

    render: function() {
        return (
            <span>{i18n.t('todo-list')} <Badge>{this.state.todos.length}</Badge></span>
        );
    }
});

var TodoList = React.createClass({

    mixins: [FluxMixin, StoreWatchMixin('todo')],

    contextTypes: {
        flux: React.PropTypes.object.isRequired
    },

    getInitialState: function() {
        return {
            newText: ''
        };
    },

    getStateFromFlux: function() {
        return this.context.flux.store('todo').getState();
    },

    render: function() {
        var todos = [];

        for(var index = 0; index < this.state.todos.length; index++) {
            todos.push(
                <ListGroupItem key={'todo-' + index}>{this.state.todos[index]}</ListGroupItem>
            )
        }

        return (
            <Panel>
                <Grid fluid>
                    <Row>
                        <Col xs={10}>
                            <Input type='text' placeholder={i18n.t('new-todo')} value={this.state.newText} onChange={this.handleTextChange} onKeyDown={this.onKeyDown} />
                        </Col>

                        <Col xs={2}>
                            <ButtonGroup>
                                <Button bsStyle='primary' onClick={this.addTodo}>{i18n.t('add-todo')}</Button>
                                <Button bsStyle='warning' onClick={this.clearTodos}>{i18n.t('clear-todo-list')}</Button>
                            </ButtonGroup>
                        </Col>
                    </Row>

                    <Row>
                        <Col xs={12}>
                            {todos.length > 0
                                ?   <ListGroup>
                                        {todos}
                                    </ListGroup>
                                :   <p>{i18n.t('empty-todo-list')}</p>
                            }
                        </Col>
                    </Row>
                </Grid>
            </Panel>
        );
    },

	onKeyDown: function(event) {
		if(event.keyCode == 13) {
			this.addTodo(event);
		}
	},

    addTodo: function(event) {
        if(this.state.newText) {
            this.context.flux.actions.todo.addTodo(this.state.newText);
            notification.showLocalizedMessage('success', 'todo-added-title', 'todo-added', {text: this.state.newText});
            this.setState({
                newText: ''
            });
        }
    },

    clearTodos: function(event) {
        if(this.state.todos.length > 0) {
            this.context.flux.actions.todo.clearTodos();
            notification.showLocalizedMessage('success', 'todo-list-cleared-title', 'todo-list-cleared');
        } else {
            notification.showLocalizedMessage('warning', 'empty-todo-list-for-clear-title', 'empty-todo-list-for-clear');
        }
    },

    handleTextChange: function(event) {
        this.setState({
            newText: event.target.value
        });
    }
});

module.exports = {title: TodoListTitle, main: TodoList};
