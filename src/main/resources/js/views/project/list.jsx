var ProjectListTitle = React.createClass({

	render: function() {
		return (
			<span>
				{i18n.t('example-project-list')}
			</span>
		);
	}
});

var ProjectList = React.createClass({
	renderTitle: function(project) {
		var label = !project ? i18n.t('example-project-detail-new') : i18n.t('example-project-detail', {name: project.name}) ;

		return (
			<span>
				{label}
			</span>
		);
	},

	validateForm: function(data, callback) {
		// here some validation and notification can be added

		callback(true);
	},

	getInitialValue: function() {
		return {
			name: 'Project ' + new Date(),
			description: 'Some nice description is here'
		};
	},

	render: function() {
		return (
			<div>
				<SW.List name='example-project' rest='/api/example/projects'>
					<SW.List.Table>
						<SW.List.String property='name' width='15%' pathProperty='id'/>
						<SW.List.String property='description' width='85%'/>
					</SW.List.Table>

					<SW.List.ActivitiesGroup>
						<SW.List.LinkModal icon='plus' labelKey='test-create-new'/>
					</SW.List.ActivitiesGroup>

					<SW.List.DetailModal renderTitle={this.renderTitle}>
						<SW.List.FormModal rest='/api/example/projects' labelKeyPrefix='example-project' validate={this.validateForm} initialValue={this.getInitialValue()}>
							<SW.Form.Section>
								<SW.Form.String size={6} property='name' validations="required;length(50)"/>
								<SW.Form.String size={12} property='description' type='area' validations='required'/>
							</SW.Form.Section>
						</SW.List.FormModal>
					</SW.List.DetailModal>

					<SW.List.QuickSearch />
					<SW.List.Pagination />
					<SW.List.Info />
				</SW.List>
			</div>
		);
	}
});

module.exports = {title: ProjectListTitle, main: ProjectList};
