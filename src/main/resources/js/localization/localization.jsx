var Cs = require('./cs.jsx');
var En = require('./en.jsx');
var Localization = {
	en: En,
	cs: Cs
};

module.exports = Localization;
