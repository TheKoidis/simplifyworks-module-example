var Localization = {
	translation: {
		'todo-list': 'Todo list',
		'new-todo': 'New todo ...',
		'add-todo': 'Add todo',
		'clear-todo-list': 'Clear all todos',
		'empty-todo-list': 'There are no todos!',

		'todo-added-title': 'Todo added',
		'todo-added': 'Todo \"{{text}}\" succesfully added to the list!',

		'todo-list-cleared-title': 'Todos cleared',
		'todo-list-cleared': 'All todos were succesfully removed from list!',

		'empty-todo-list-for-clear-title': 'No todos',
		'empty-todo-list-for-clear': 'Cannot clear empty todo list!',

		'example-issue-list': 'Issue list',
		'example-issue-title': 'Title',
		'example-issue-title-tooltip': 'Title of the issue',
		'example-issue-title-placeholder': 'Enter title of the issue',
		'example-issue-title-help': 'Short title of the issue',
		'example-issue-dueDate': 'Due date',
		'example-issue-priority': 'Priority',
		'example-issue-internal': 'Internal',
		'example-issue-public': 'Public',
		'example-issue-price': 'Price',
		'example-issue-price-sum': 'Sum',
		'example-issue-price-avg': 'Average',
		'example-issue-created': 'Created',
		'example-issue-project-name': 'Project',
		'example-issue-project': 'Project',
		'example-issue-description': 'Description',
		'example-issue-reportedVersion': 'Version',
		'example-issue-fixVersion': 'Fix version',
		'example-issue-environments': 'Environments',
		'example-issue-components': 'Components',
		'example-issue-labels': 'Labels',
		'example-issue-tasks': 'Tasks',
		'example-issue-comments': 'Comments',
		'example-issue-attachments': 'Attachments',
		'example-issue_attachment_unsupported': 'Attachment must be image',

		'example-issue-detail': 'Detail of issue - {{title}}',
		'example-issue-new': 'New issue',
		'example-issue-base-info': 'Base info',
		'example-issue-base-info-tooltip': 'Base info tab',
		'example-issue-additional-info': 'Additional info',
		'example-issue-additional-info-tooltip': 'Additional info tab',

		'example-issue-filter-title': 'Title',
		'example-issue-filter-price': 'Price',
		'example-issue-filter-priority': 'Priority',
		'example-issue-filter-internal': 'Internal',
		'example-issue-filter-minPrice': 'Price (from)',
		'example-issue-filter-maxPrice': 'Price (to)',
		'example-issue-filter-fromDate': 'Due date (from)',
		'example-issue-filter-toDate': 'Due date (to)',
		'example-issue-filter-project': 'Project',

		'example-task': 'Task',
		'example-task-type': 'Type',
		'example-task-estimation': 'Estimation (minutes)',
		'example-task-description': 'Description',
		'example-task-dueDate': 'Due date',
		'example-task-resolution': 'Resolution',

		'example-resolution': 'Resolution',
		'example-resolution-result': 'Result',
		'example-resolution-comment': 'Comment',
		'example-resolution-finished': 'Finished',

		'example-project': 'Projekt',
		'example-project-name': 'Name',
		'example-project-description': 'Description',
		'example-project-list': 'Project list',
		'example-project-detail': 'Detail of project - {{name}}',
		'example-project-detail-new': 'New project',

		'example-component': 'Component',
		'example-component-name': 'Name',
		'example-component-description': 'Description',

		'example-comment-nick': 'Nickname',
		'example-comment-description': 'Comment text',
		'example-comment-time': 'Post time',

		'example-dropdown': 'Issues and projects',

		'example-attachment': 'Attachment',
		'example-attachment-detail': 'Detail',
		'example-attachment-name': 'Name',
		'example-attachment-size': 'Size',
		'example-attachment-uploaded': 'Uploaded',
		'example-attachment-description': 'Description',

		//---Test---
		'test-create-new': 'Create new',
		'test-action-menu': 'Menu 1',
		'test-action-1': 'Action 1',
		'test-action-2': 'Action 2',
		'test-action-3': 'Action 3',
		'test-filter-menu': 'Filter menu',
		'test-filter-1': 'Filter 1',
		'test-filter-2': 'Filter 2',
	}
};

module.exports = Localization;
