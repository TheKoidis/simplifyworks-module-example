var Localization = {
	translation: {
		'todo-list': 'Seznam úkolů',
		'new-todo': 'Nový úkol ...',
		'add-todo': 'Přidat úkol',
		'clear-todo-list': 'Smazat všechny úkoly',
		'empty-todo-list': 'Seznam úkolů je prázdný!',

		'todo-added-title': 'Úkol přidán',
		'todo-added': 'Úkol \"{{text}}\" úspěšně přidán do seznamu!',

		'todo-list-cleared-title': 'Seznam úkolů smazán',
		'todo-list-cleared': 'Všechny úkoly byly úspěšně smazány ze seznamu!',

		'empty-todo-list-for-clear-title': 'Žádné úkoly',
		'empty-todo-list-for-clear': 'Nejsou žádné úkoly ke smazání!',

		'example-issue-list': 'Seznam incidentů',
		'example-issue-title': 'Titulek',
		'example-issue-title-tooltip': 'Titulek incidentu',
		'example-issue-title-placeholder': 'Zadejte titulek incidentu',
		'example-issue-title-help': 'Krátký titulek incidentu',
		'example-issue-dueDate': 'Termín',
		'example-issue-priority': 'Priorita',
		'example-issue-internal': 'Interní',
		'example-issue-public': 'Veřejný',
		'example-issue-price': 'Cena',
		'example-issue-price-sum': 'Suma',
		'example-issue-price-avg': 'Průměr',
		'example-issue-created': 'Vytvořeno',
		'example-issue-project-name': 'Projekt',
		'example-issue-project': 'Projekt',
		'example-issue-description': 'Popis',
		'example-issue-reportedVersion': 'Verze',
		'example-issue-fixVersion': 'Opraveno ve verzi',
		'example-issue-environments': 'Prostředí',
		'example-issue-components': 'Komponenty',
		'example-issue-labels': 'Štítky',
		'example-issue-tasks': 'Úkoly',
		'example-issue-comments': 'Komentáře',
		'example-issue-attachments': 'Přílohy',
		'example-issue_attachment_unsupported': 'Příloha musí být obrázek',

		'example-issue-detail': 'Detail incidentu - {{title}}',
		'example-issue-new': 'Nový incident',
		'example-issue-base-info': 'Základní údaje',
		'example-issue-base-info-tooltip': 'Záložka se základními údaji',
		'example-issue-additional-info': 'Rozšířené údaje',
		'example-issue-additional-info-tooltip': 'Záložka s rozšířujícími údaji',

		'example-issue-filter-title': 'Titulek',
		'example-issue-filter-price': 'Cena',
		'example-issue-filter-priority': 'Priorita',
		'example-issue-filter-internal': 'Interní',
		'example-issue-filter-minPrice': 'Cena (od)',
		'example-issue-filter-maxPrice': 'Cena (do)',
		'example-issue-filter-fromDate': 'Termín (od)',
		'example-issue-filter-toDate': 'Termín (do)',
		'example-issue-filter-project': 'Projekt',

		'example-task': 'Úkol',
		'example-task-type': 'Typ',
		'example-task-estimation': 'Odhad (minut)',
		'example-task-description': 'Popis',
		'example-task-dueDate': 'Termín',
		'example-task-resolution': 'Řešení',

		'example-resolution': 'Řešení',
		'example-resolution-result': 'Výsledek',
		'example-resolution-comment': 'Komentář',
		'example-resolution-finished': 'Dokončeno',

		'example-project': 'Projekt',
		'example-project-name': 'Jméno',
		'example-project-description': 'Popis',
		'example-project-list': 'Seznam projektů',
		'example-project-detail': 'Detail projektu - {{name}}',
		'example-project-detail-new': 'Nový projekt',

		'example-component': 'Komponenta',
		'example-component-name': 'Jméno',
		'example-component-description': 'Popis',

		'example-comment-nick': 'Přezdívka',
		'example-comment-description': 'Text komentáře',
		'example-comment-time': 'Čas vložení',

		'example-dropdown': 'Incidenty a projekty',

		'example-attachment': 'Příloha',
		'example-attachment-detail': 'Detail',
		'example-attachment-name': 'Jméno',
		'example-attachment-size': 'Velikost',
		'example-attachment-uploaded': 'Nahráno',
		'example-attachment-description': 'Popis',

		//---Test---
		'test-create-new': 'Vytvořit nový',
		'test-action-menu': 'Menu 1',
		'test-action-1': 'Akce 1',
		'test-action-2': 'Akce 2',
		'test-action-3': 'Akce 3',
		'test-filter-menu': 'Filtr menu',
		'test-filter-1': 'Filtr 1',
		'test-filter-2': 'Filtr 2',
	}
}

module.exports = Localization;
