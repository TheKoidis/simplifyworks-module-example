var constants = require('./../constants/todo-constants.jsx');

var TodoStore = Fluxxor.createStore({

    initialize: function() {
        if(!localStorage.getItem('todos')) {
            localStorage.setItem('todos', JSON.stringify([]));
        }

        this.bindActions(
            constants.TODO_ADD, this.addTodo,
            constants.TODO_CLEAR, this.clearTodos
        );
    },

    getState: function() {
        return {
            todos: JSON.parse(localStorage.getItem('todos'))
        };
    },

    addTodo: function(payload) {
        var todos = JSON.parse(localStorage.getItem('todos'));
        todos.push(payload.text);
        localStorage.setItem('todos', JSON.stringify(todos));

        this.emit('change');
    },

    clearTodos: function(payload) {
        localStorage.setItem('todos', JSON.stringify([]));

        this.emit('change');
    }
});

module.exports = TodoStore;
