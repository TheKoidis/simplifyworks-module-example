var constants = require('./../constants/todo-constants.jsx');

var TodoActions = {

    addTodo: function(text) {
        this.dispatch(constants.TODO_ADD, {text: text})
    },

    clearTodos: function() {
        this.dispatch(constants.TODO_CLEAR, {})
    }
};

module.exports = TodoActions;
