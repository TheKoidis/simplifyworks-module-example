var TodoActions = require('./actions/todo-actions.jsx');
var TodoStore = require('./stores/todo-store.jsx');
var TodoList = require('./views/todo-list.jsx');
var IssueList = require('./views/issue/list.jsx');
var IssueDetail = require('./views/issue/detail.jsx');
var ProjectList = require('./views/project/list.jsx');
var localization = require('./localization/localization.jsx');

var menu = {
	'todoList': {icon: 'tasks', labelKey: 'todo-list', path: '/example/todo-list'},
	'exampleList':{
		icon: 'option-vertical', labelKey: 'example-dropdown',
		items: {'issueList': {icon: 'th-list', labelKey: 'example-issue-list', path: '/example/issues'},
				'projectList': {icon: 'list', labelKey: 'example-project-list', path: '/example/projects'}}
	}
};

var routes = (
	<Route key='example' name='example-module' path='/example/'>
		<Route key='todo-list' path='todo-list' components={{title: TodoList.title, main: TodoList.main}} />
		<Route key='example-issue-list' path='issues' components={{title: IssueList.title, main: IssueList.main}} />
		<Route key='example-issue-detail' path='issue/:issueId' components={{title: IssueDetail.title, main: IssueDetail.main}} />
		<Route key='example-project-list' path='projects' components={{title: ProjectList.title, main: ProjectList.main}} />
	</Route>
);

var Module = {
	name: 'Todo',
	actions: {todo: TodoActions},
	stores: {todo: new TodoStore()},
	menu: menu,
	routes: routes,
	localization: localization
}

module.exports = Module;
