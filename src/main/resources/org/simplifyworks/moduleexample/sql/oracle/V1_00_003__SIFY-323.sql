-- SIFY-323: Mandatory created and creator attributes
begin
  for rec in (select c.COLUMN_NAME, c.TABLE_NAME
                from user_tab_columns c, user_tables t
               where t.table_name like 'EXA%'
                 and (column_name = 'CREATED' or column_name = 'CREATOR')
                 and c.nullable = 'Y'
                 and c.table_name = t.table_name) loop
    if (rec.column_name = 'CREATOR') then
      execute immediate 'update ' || rec.table_name || ' set creator=''SIFY-323'' where creator is null';
    elsif (rec.column_name = 'CREATED') then
      execute immediate 'update ' || rec.table_name || ' set created=nvl(modified,sysdate) where created is null';
    end if;
    execute immediate 'alter table ' || rec.table_name || ' modify ' || rec.column_name || ' not null';
  end loop;
  commit;
end;
