DROP TABLE exa_attachment;

CREATE TABLE exa_attachment (
  id number(20) NOT NULL,
  issue_id number(20) NOT NULL,
  description varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

alter table EXA_ATTACHMENT
  add constraint EXA_ATTACHMENT_ISS_FK foreign key (ISSUE_ID)
  references exa_issue (ID);

alter table EXA_ATTACHMENT
  add constraint EXA_ATTACHMENT_CORE_FK foreign key (ID)
  references core_attachment (ID);



