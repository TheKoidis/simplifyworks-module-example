CREATE TABLE exa_attachment (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  mimetype varchar(255)  NOT NULL,
  name varchar(255)  NOT NULL,
  filesize number(20) NOT NULL,
  "UID" varchar(36)  NOT NULL,
  uploaded date DEFAULT NULL,
  issue_id number(20) NOT NULL,
  PRIMARY KEY (id)
) ;

CREATE TABLE exa_comment (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  description varchar(2000)  NOT NULL,
  nick varchar(100)  DEFAULT NULL,
  time date DEFAULT NULL,
  issue_id number(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE exa_component (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  description varchar(2000)  DEFAULT NULL,
  name varchar(255)  NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE exa_issue (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  description varchar(2000)  NOT NULL,
  due_date date DEFAULT NULL,
  fix_version varchar(10)  DEFAULT NULL,
  internal number(1) NOT NULL,
  price decimal(14,2) DEFAULT NULL,
  priority varchar(255)  NOT NULL,
  reported_version varchar(10)  DEFAULT NULL,
  reporter varchar(255)  DEFAULT NULL,
  title varchar(50)  NOT NULL,
  project_id number(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE exa_issue_components (
  exa_issue_ID number(20) NOT NULL,
  components_ID number(20) NOT NULL,
  PRIMARY KEY (exa_issue_ID,components_ID)
) ;

CREATE TABLE exa_issue_environments (
  issue_ID number(20) NOT NULL,
  environments varchar(255)  DEFAULT NULL
) ;

CREATE TABLE exa_issue_labels (
  issue_ID number(20) NOT NULL,
  labels varchar(255)  DEFAULT NULL
) ;

CREATE TABLE exa_project (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  description varchar(2000)  DEFAULT NULL,
  name varchar(255)  NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE exa_resolution (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  "COMMENT" varchar(2000)  DEFAULT NULL,
  finished date DEFAULT NULL,
  result varchar(255)  NOT NULL,
  task_id number(20) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE exa_task (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  description varchar(2000)  NOT NULL,
  due_date date DEFAULT NULL,
  estimation number(11) DEFAULT NULL,
  type varchar(255)  NOT NULL,
  issue_id number(20) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO exa_project (id,name,description) VALUES (1,'SWF', 'SimplifyWorks framework');
INSERT INTO exa_project (id,name,description) VALUES (2,'CON', 'Contracts');
INSERT INTO exa_project (id,name,description) VALUES (3,'EXA', 'Examples');
INSERT INTO exa_project (id,name,description) VALUES (4,'EDU', 'Education');
INSERT INTO exa_project (id,name,description) VALUES (5,'OUT', 'Outsourcing');
INSERT INTO exa_project (id,name,description) VALUES (6,'FIN', 'Finances');
INSERT INTO exa_project (id,name,description) VALUES (7,'ACC', 'Accounting');
INSERT INTO exa_project (id,name,description) VALUES (8,'RES', 'Reservations');
INSERT INTO exa_project (id,name,description) VALUES (9,'EMP', 'Employees');
INSERT INTO exa_project (id,name,description) VALUES (10,'REP', 'Reports');
INSERT INTO exa_project (id,name,description) VALUES (11,'BPR', 'Business processes');

INSERT INTO exa_component (id,name,description) VALUES (1,'Workflow', 'Workflow');
INSERT INTO exa_component (id,name,description) VALUES (2,'User access', 'User access');
INSERT INTO exa_component (id,name,description) VALUES (3,'User interface', 'User interface');
INSERT INTO exa_component (id,name,description) VALUES (4,'REST', 'REST Web Services');
INSERT INTO exa_component (id,name,description) VALUES (5,'SOAP', 'SOAP Web Services');
INSERT INTO exa_component (id,name,description) VALUES (6,'Logging', 'Logging');
INSERT INTO exa_component (id,name,description) VALUES (7,'Database', 'Databases');
INSERT INTO exa_component (id,name,description) VALUES (8,'Java', 'Java');
INSERT INTO exa_component (id,name,description) VALUES (9,'JavaScript', 'JavaScript');
INSERT INTO exa_component (id,name,description) VALUES (10,'SQL', 'SQL');
INSERT INTO exa_component (id,name,description) VALUES (11,'Server', 'Server communication');
INSERT INTO exa_component (id,name,description) VALUES (12,'Client', 'Client communication');
commit;

alter table EXA_ATTACHMENT
  add constraint EXA_ATTACHMENT_ISS_FK foreign key (ISSUE_ID)
  references exa_issue (ID);
alter table exa_comment
  add constraint exa_comment_ISS_FK foreign key (ISSUE_ID)
  references exa_issue (ID);
alter table exa_issue
  add constraint exa_issue_PROJ_FK foreign key (project_id)
  references exa_project (ID);
alter table exa_issue_components
  add constraint exa_issue_component_FK foreign key (components_ID)
  references exa_component (ID);
alter table exa_issue_environments
  add constraint exa_issue_environments_FK foreign key (issue_ID)
  references exa_issue (ID);
alter table exa_issue_labels
  add constraint exa_issue_labels_FK foreign key (issue_ID)
  references exa_issue (ID);
alter table exa_task
  add constraint exa_task_FK foreign key (issue_ID)
  references exa_issue (ID);
alter table EXA_RESOLUTION
  add constraint EXA_RESOLUTION unique (TASK_ID);
