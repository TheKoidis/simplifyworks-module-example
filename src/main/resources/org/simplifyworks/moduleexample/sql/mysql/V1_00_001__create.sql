CREATE TABLE `exa_attachment` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `mimetype` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `filesize` bigint(18) unsigned NOT NULL,
  `uid` varchar(36) COLLATE utf8_czech_ci NOT NULL,
  `uploaded` datetime DEFAULT NULL,
  `issue_id` bigint(18) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cfq5q08jxsgs76if0kfaam1d6` (`issue_id`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_comment` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_czech_ci NOT NULL,
  `nick` varchar(100) COLLATE utf8_czech_ci DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `issue_id` bigint(18) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_dywir2i105wmu4ojtr8e0wkw5` (`issue_id`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_component` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_issue` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_czech_ci NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `fix_version` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `internal` bit(1) NOT NULL,
  `price` decimal(14,2) DEFAULT NULL,
  `priority` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `reported_version` varchar(10) COLLATE utf8_czech_ci DEFAULT NULL,
  `reporter` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8_czech_ci NOT NULL,
  `project_id` bigint(18) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_q1715woxsr13f6gndwhyj93k8` (`project_id`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_issue_components` (
  `exa_issue_ID` bigint(18) unsigned NOT NULL,
  `components_ID` bigint(18) unsigned NOT NULL,
  PRIMARY KEY (`exa_issue_ID`,`components_ID`),
  KEY `FK_d1qk6nuhy6whfm9xasljkt92k` (`components_ID`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_issue_environments` (
  `issue_ID` bigint(18) unsigned NOT NULL,
  `environments` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  KEY `FK_t284d5mc15h1y4jflvnub7se4` (`issue_ID`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_issue_labels` (
  `issue_ID` bigint(18) unsigned NOT NULL,
  `labels` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  KEY `FK_n22k939l7d3uneg0s663jkb36` (`issue_ID`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_project` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  PRIMARY KEY (`id`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_resolution` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `comment` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL,
  `finished` datetime DEFAULT NULL,
  `result` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `task_id` bigint(18) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8rtulywgdmy4jktbdta0r466w` (`task_id`),
  UNIQUE KEY `UK_6mynjfstokqg4c6mk3xits5m4` (`task_id`)
) engine=innodb default charset=utf8 collate utf8_bin;

CREATE TABLE `exa_task` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `creator` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_czech_ci NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `estimation` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `issue_id` bigint(18) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jvq1o0sixj5e16r8s5j13jv1r` (`issue_id`)
) engine=innodb default charset=utf8 collate utf8_bin;

INSERT INTO `exa_project` (`name`,`description`) VALUES ("SWF", "SimplifyWorks framework");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("CON", "Contracts");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("EXA", "Examples");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("EDU", "Education");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("OUT", "Outsourcing");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("FIN", "Finances");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("ACC", "Accounting");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("RES", "Reservations");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("EMP", "Employees");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("REP", "Reports");
INSERT INTO `exa_project` (`name`,`description`) VALUES ("BPR", "Business processes");

INSERT INTO `exa_component` (`name`,`description`) VALUES ("Workflow", "Workflow");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("User access", "User access");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("User interface", "User interface");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("REST", "REST Web Services");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("SOAP", "SOAP Web Services");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("Logging", "Logging");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("Database", "Databases");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("Java", "Java");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("JavaScript", "JavaScript");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("SQL", "SQL");
INSERT INTO `exa_component` (`name`,`description`) VALUES ("Server", "Server communication");
INSERT INTO `exa_component` (`name`,`description`) VALUES ( "Client", "Client communication");

