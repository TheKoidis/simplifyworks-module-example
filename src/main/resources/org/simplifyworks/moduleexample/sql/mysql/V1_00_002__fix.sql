alter table `exa_resolution` change column `comment` `note` varchar(2000) COLLATE utf8_czech_ci DEFAULT NULL;
alter table `exa_attachment` change column `uid` `guid` varchar(36) COLLATE utf8_czech_ci NOT NULL;
