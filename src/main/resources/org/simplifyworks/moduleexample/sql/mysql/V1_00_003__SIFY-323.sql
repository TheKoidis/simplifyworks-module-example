-- SIFY-323: mandatory created and creator attributes
update EXA_ATTACHMENT set created=coalesce(modified,sysdate()) where created is null;
update EXA_ATTACHMENT set creator='SIFY-323' where creator is null;
alter table EXA_ATTACHMENT modify created datetime not null;
alter table EXA_ATTACHMENT modify creator varchar(50) not null;

update EXA_COMMENT set created=coalesce(modified,sysdate()) where created is null;
update EXA_COMMENT set creator='SIFY-323' where creator is null;
alter table EXA_COMMENT modify created datetime not null;
alter table EXA_COMMENT modify creator varchar(50) not null;

update EXA_COMPONENT set created=coalesce(modified,sysdate()) where created is null;
update EXA_COMPONENT set creator='SIFY-323' where creator is null;
alter table EXA_COMPONENT modify created datetime not null;
alter table EXA_COMPONENT modify creator varchar(50) not null;

update EXA_ISSUE set created=coalesce(modified,sysdate()) where created is null;
update EXA_ISSUE set creator='SIFY-323' where creator is null;
alter table EXA_ISSUE modify created datetime not null;
alter table EXA_ISSUE modify creator varchar(50) not null;

update EXA_PROJECT set created=coalesce(modified,sysdate()) where created is null;
update EXA_PROJECT set creator='SIFY-323' where creator is null;
alter table EXA_PROJECT modify created datetime not null;
alter table EXA_PROJECT modify creator varchar(50) not null;

update EXA_RESOLUTION set created=coalesce(modified,sysdate()) where created is null;
update EXA_RESOLUTION set creator='SIFY-323' where creator is null;
alter table EXA_RESOLUTION modify created datetime not null;
alter table EXA_RESOLUTION modify creator varchar(50) not null;

update EXA_TASK set created=coalesce(modified,sysdate()) where created is null;
update EXA_TASK set creator='SIFY-323' where creator is null;
alter table EXA_TASK modify created datetime not null;
alter table EXA_TASK modify creator varchar(50) not null;

commit;
