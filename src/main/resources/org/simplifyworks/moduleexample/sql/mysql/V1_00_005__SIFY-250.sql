DROP TABLE `exa_attachment`;

CREATE TABLE `exa_attachment` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `issue_id` bigint(18) unsigned NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `EXA_ATT_ISSUE_IX` (`issue_id`),
  CONSTRAINT `fk_exa_att_issue` FOREIGN KEY (`issue_id`) REFERENCES `exa_issue` (`id`),
  CONSTRAINT `fk_exa_att_core_att` FOREIGN KEY (`id`) REFERENCES `core_attachment` (`id`)
) engine=innodb default charset=utf8 collate utf8_bin;


